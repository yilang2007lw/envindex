from django import forms

class SearchForm(forms.Form):
    start = forms.DateField(required=False)
    end = forms.DateField(required=False)
    keywords = forms.CharField()
