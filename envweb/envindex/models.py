from django.db import models

class Info(models.Model):
    title = models.CharField(max_length=1024)
    link = models.CharField(max_length=1024)
    date = models.DateField()
    province = models.CharField(max_length=20)
    city = models.CharField(max_length=20)

    def __unicode__(self):
        return self.title
