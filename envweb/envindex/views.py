#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext
from forms import SearchForm
from models import Info

# Create your views here.
provincelist = ["北京","广东","上海","天津","重庆","辽宁","江苏","湖北","四川","陕西","河北",
            "山西","河南","吉林","黑龙江","内蒙古","山东","安徽","浙江","福建","湖南",
            "广西","江西","贵州","云南","西藏","海南","甘肃","宁夏","青海","新疆","香港","澳门","台湾"]

def _construct_env_charts(env):
    env["bar"] = {}
    env["bar"]["legend_data"] = []
    env["bar"]["series_data"] = []

    env["line"] = {}
    env["line"]["legend_data"] = []
    env["line"]["series_data"] = []

    env["pie"] = {}
    env["pie"]["title"] = "环保指数展示"
    env["pie"]["sub_title"] = "饼状图"
    env["pie"]["legend_data"] = []
    env["pie"]["series_data"] = []

    env["map"] = {}
    env["map"]["title"] = "环保指数展示"
    env["map"]["sub_title"] = "省份图"
    #env["map"]["max"] = 5000
    env["map"]["legend_data"] = []
    env["map"]["series_data"] = []

    return env

def _query_env_data(env, start, end , keywords):
    if not start:
        start = "1949-10-01"
    if not end:
        end = "2038-01-01"

    for q in keywords:
        env["bar"]["legend_data"].append(q)
        env["line"]["legend_data"].append(q)
        c_2008 = len(Info.objects.filter(date__year = 2008))
        c_2009 = len(Info.objects.filter(date__year = 2009))
        c_2010 = len(Info.objects.filter(date__year = 2010))
        c_2011 = len(Info.objects.filter(date__year = 2011))
        c_2012 = len(Info.objects.filter(date__year = 2012))
        c_2013 = len(Info.objects.filter(date__year = 2013))
        c_2014 = len(Info.objects.filter(date__year = 2014))

        q_bar_data = {}
        q_bar_data["name"] = q
        q_bar_data["type"] = "bar"
        q_bar_data["data"] = [c_2008, c_2009, c_2010, c_2011, c_2012, c_2013, c_2014]
        env["bar"]["series_data"].append(q_bar_data)

        q_line_data = {}
        q_line_data["name"] = q
        q_line_data["type"] = "line"
        q_line_data["stack"] = "count"
        q_line_data["data"] = [c_2008, c_2009, c_2010, c_2011, c_2012, c_2013, c_2014]
        env["line"]["series_data"].append(q_line_data)
        q_data = Info.objects.filter(date__range=(start, end), title__icontains=q)

        env["pie"]["legend_data"].append(q)
        env["pie"]["series_data"].append({"name":q, "value":len(q_data)})

        env["map"]["legend_data"].append(q)
        q_map_data = {}
        q_map_data["name"] = q
        q_map_data["data"] = []
        map_max = 0
        for p in provincelist:
            p_count = 0
            for i in q_data:
                p_count += i.title.count(p.decode("utf-8"))
            if p_count > map_max:
                map_max = p_count
            q_map_data["data"].append({"name":p, "value":p_count})
        env["map"]["max"] = map_max 
        env["map"]["series_data"].append(q_map_data)
    return env

def home(request):
    env = {}
    if request.method == "POST":
        form = SearchForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            start = cd["start"]
            end = cd["end"]
            keywords = cd["keywords"].split(" ")
            env = _construct_env_charts(env)
            env = _query_env_data(env, start, end, keywords)
    else:
        form = SearchForm()
    return render_to_response("index.html", {'form':form, 'env':env}, context_instance = RequestContext(request))
