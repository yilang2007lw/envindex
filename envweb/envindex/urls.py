from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^search$', include('haystack.urls')),
    url(r'^$', "envindex.views.home"),
)
