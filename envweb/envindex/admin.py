from django.contrib import admin

from models import Info

class InfoAdmin(admin.ModelAdmin):
    list_display = ('title', 'link', 'province', 'city', 'date')
    search_fields = ('title', 'province', 'city')
    list_filter = ('date', 'province')
    ordering = ('-date',)


admin.site.register(Info, InfoAdmin)
