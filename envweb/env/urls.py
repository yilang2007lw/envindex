from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^envindex/', include("envindex.urls")),
    #url(r'^$', 'envindex.views.home', name='home'),
)
