# -*- coding: utf-8 -*-
import scrapy

from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city

class SinaSpider(scrapy.Spider):
    name = "sina"
    allowed_domains = ["roll.green.sina.com.cn"]
    start_urls = (
        'http://roll.green.sina.com.cn/green/hb_gdxw/index.shtml',
    )

    def parse(self, response):
        try:
            newslistlb = response.css('ul[class="list_009"]')
            for li in newslistlb.xpath('li'):
                item = EnvItem()
                item["link"] = li.xpath("a/@href").extract()[0]
                item["title"] = li.xpath("a/text()").extract()[0]
                item["date"] = str(item["link"].split("/")[-2])
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item
            a = response.css('span[class="pagebox_next"]')
            if a:
                first_url = "/".join(response.url.split("/")[:-1])
                last_url = a.xpath("a/@href").extract()[0].split("/")[-1]
                url = first_url + "/" + last_url;
                yield Request(url, callback = self.parse)
        except:
            pass


