# -*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city


class IfengSpider(scrapy.Spider):
    name = "ifeng"
    allowed_domains = ["ifeng.com"]
    start_urls = (
            'http://green.gongyi.ifeng.com/hbyw/list_0/0.shtml',
            'http://green.gongyi.ifeng.com/hbsd/list_0/0.shtml',
            'http://green.gongyi.ifeng.com/lsst/list_0/0.shtml',
            'http://green.gongyi.ifeng.com/gtfw/list_0/0.shtml',
            'http://green.gongyi.ifeng.com/wrfz/list_0/0.shtml',
            'http://green.gongyi.ifeng.com/hbbgt/list_0/0.shtml',
    )

    def parse(self, response):
        try:
            a = response.css('div[class="newsList"]').css('a[target="_blank"]')
            for index in a:
                print index.xpath("@href").extract()[0]
                item = EnvItem()
                item["link"] = index.xpath("@href").extract()[0]
                date_info = item["link"].split("/")
                item["date"] = date_info[4].split("_")[1] + '-' + date_info[4].split("_")[2] + '-' + date_info[5]
                item["title"] = index.xpath("text()").extract()[0]
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item
            mypage = response.css('div[class="m_page"]')
            url = mypage.xpath('span')[-1].xpath("a/@href").extract()[0]
            yield Request(url, callback = self.parse)
        except:
            pass
