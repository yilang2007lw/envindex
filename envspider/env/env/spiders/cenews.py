# -*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city


class CenewsSpider(scrapy.Spider):
    name = "cenews"
    allowed_domains = ["cenews.com.cn"]
    start_urls = (
            #'http://www.cenews.com.cn/xwzx2013/wrfz/',
            #'http://www.cenews.com.cn/xwzx2013/jnjp/',
            #'http://www.cenews.com.cn/xwzx2013/zcjd/',
            #'http://www.cenews.com.cn/xwzx2013/hjyw/',
            #'http://www.cenews.com.cn/xwzx2013/bwdt/',
            #'http://www.cenews.com.cn/xwzx2013/qt/',
            #'http://www.cenews.com.cn/xwzx2013/hbzx/',
            #'http://www.cenews.com.cn/xwzx2013/hjzt/',
            #'http://www.cenews.com.cn/xwzx2013/hjsj/',
            #'http://www.cenews.com.cn/xwzx2013/zrst/',
            #'http://www.cenews.com.cn/xwzx2013/hjjj/',
            #'http://www.cenews.com.cn/xwzx2013/qhbh/',
            #'http://www.cenews.com.cn/xwzx2013/stwm/',
            #'http://www.cenews.com.cn/xwzx2013/dt/',
            #'http://www.cenews.com.cn/xwzx2013/ny/',
            #'http://www.cenews.com.cn/xwzx2013/xxgk/',
            #'http://www.cenews.com.cn/xwzx2013/hyfs/',
            'http://www.cenews.com.cn/xwzx2013/szhb/',
    )

    def parse(self, response):
        try:
            new_list = response.css('ul[class="new_list"]')
            for li in new_list.xpath('li'):
                item = EnvItem()
                item["date"] = li.xpath("span/text()").extract()[0]
                item["link"] = self.start_urls[0] + li.xpath("a/@href").extract()[0].strip(".")
                item["title"] = li.xpath("a/text()").extract()[0]
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item

            pagenum = response.css('div[class="yema"]').xpath('script').xpath("text()").extract()[0].split(";")[0].split("=")[1]
            url = self.start_urls[0] + "index_" + str(int(pagenum) + 1) + ".html"
            yield Request(url, callback = self.parse)
        except:
            pass
