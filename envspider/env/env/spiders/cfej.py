# -*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city

class CfejSpider(scrapy.Spider):
    name = "cfej"
    allowed_domains = ["www.cfej.net"]
    start_urls = (
        # 'http://www.cfej.net/?list-50.html',
         'http://www.cfej.net/?list-49.html',
        #'http://www.cfej.net/?list-47.html',
        #'http://www.cfej.net/?list-46.html',
        #'http://www.cfej.net/?list-45.html',
        #'http://www.cfej.net/?list-44.html',
        #'http://www.cfej.net/?list-43.html',
        #'http://www.cfej.net/?list-71.html',
        #'http://www.cfej.net/?list-72.html',
        #'http://www.cfej.net/?list-73.html',
        #'http://www.cfej.net/?list-96.html',
    )

    def parse(self, response):
        try:
            newslistlb = response.css('div[class="newslistlb"]')
            for li in newslistlb.xpath('li'):
                item = EnvItem()
                date_info = li.xpath("span/text()").extract()[0].split(".")
                item["date"] = date_info[0] + '-' + date_info[1] + '-' + date_info[2]
                item["link"] = li.xpath("a/@href").extract()[0]
                item["title"] = li.xpath("a/text()").extract()[0]
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item
            a = response.css('a[class="next"]')
            if a:
                url = "/".join(response.url.split("/")[:-1]) + "/" + a.xpath("@href").extract()[-1]
                yield Request(url, callback = self.parse)
        except:
            pass
