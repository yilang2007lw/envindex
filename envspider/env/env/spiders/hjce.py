# -*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city

class HjceSpider(scrapy.Spider):
    name = "hjce"
    allowed_domains = ["hj.ce.cn"]
    start_urls = (
            'http://hj.ce.cn/jnjp/',
            #'http://hj.ce.cn/sthj/',
            #'http://hj.ce.cn/dtjj/',
            #'http://hj.ce.cn/wrzl/',
            #'http://hj.ce.cn/lssh/',
            #'http://hj.ce.cn/bgt/',
            #'http://hj.ce.cn/splt/',
            #'http://hj.ce.cn/hyhd/',
    )

    def parse(self, response):
        try:
            span = response.css('span[class="f1"]')
            for index in span:
                item = EnvItem()
                item['title'] = index.xpath("a/text()").extract()[0]
                link_info = index.xpath("a/@href").extract()[0].split("/")[-1].split('_')
                print link_info
                if len(link_info) < 2:
                    continue
                item['date'] = link_info[0][1:5] + '-' + link_info[0][5:7] + '-' + link_info[0][7:]
                item['link'] = "http://hj.ce.cn/" + "/".join(link_info[1:])
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item

            pagenum = response.css('div[class="setpage"]').css('script').xpath("text()").extract()[0].split('\n')[-3].split(',')[1]
            url = self.start_urls[0] + "index_" + str(int(pagenum) + 1) + ".shtml"
            yield Request(url, callback = self.parse)
        except:
            pass

