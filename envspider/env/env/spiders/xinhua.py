# -*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city


class XinhuaSpider(scrapy.Spider):
    name = "xinhua"
    allowed_domains = ["xinhuanet.com", "search.news.cn"]
    start_urls = (
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116773&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116772&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116774&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116785&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116777&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=1124006&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116775&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116776&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=1124007&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116781&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116782&nodetype=3',
            #'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=1124017&nodetype=3',
            'http://search.news.cn/mb/xinhuanet/search/?styleurl=http://www.xinhuanet.com/overseas/static/style/css_erji.css&nodeid=116784&nodetype=3',
    )


    def parse(self, response):
        try:
            table = response.css('table[class="hei14"]').css('a[class="hei14"]')
            for index in table:
                item = EnvItem()
                item['link'] = index.xpath('@href').extract()[0]
                if not index.xpath('text()'):
                        continue
                item['title'] = index.xpath('text()').extract()[0]
                link_info = item['link'].split("/")
                item['date'] = link_info[-3] + '-' + link_info[-2]
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item

            a = response.css('table[class=hei12]').css('a')
            if a:
                url = "http://search.news.cn/mb/xinhuanet/search/" + a[-2].xpath('@href').extract()[0]
                print url
                yield Request(url, callback = self.parse)
        except:
            pass
