# -*- coding: utf-8 -*-
import scrapy
import re
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city


class ChinajnhbSpider(scrapy.Spider):
    name = "chinajnhb"
    allowed_domains = ["chinajnhb.com"]
    start_urls = (
            'http://www.chinajnhb.com/NewsList/4.html',
            'http://www.chinajnhb.com/NewsList/5.html',
            'http://www.chinajnhb.com/NewsList/6.html',
            'http://www.chinajnhb.com/NewsList/7.html',
            'http://www.chinajnhb.com/NewsList/8.html',
            'http://www.chinajnhb.com/NewsList/9.html',
            'http://www.chinajnhb.com/NewsList/10.html',
            'http://www.chinajnhb.com/NewsList/11.html',
            'http://www.chinajnhb.com/NewsList/12.html',
            'http://www.chinajnhb.com/NewsList/13.html',
            'http://www.chinajnhb.com/NewsList/14.html',
            'http://www.chinajnhb.com/NewsList/15.html',
            'http://www.chinajnhb.com/NewsList/16.html',
            'http://www.chinajnhb.com/NewsList/17.html',
            'http://www.chinajnhb.com/NewsList/18.html',
            'http://www.chinajnhb.com/NewsList/19.html',
            'http://www.chinajnhb.com/NewsList/20.html',
            'http://www.chinajnhb.com/NewsList/21.html',
    )

    def parse(self, response):
        try:
            li = response.css('div[class="left_content"]').css('li')
            for index in li:
                item = EnvItem()
                date = re.findall(r'(\w*[0-9]+)\w*',(index.xpath("span/text()").extract()[0]))
                print date
                item["date"] = str("%04d-%02d-%02d"%(int(date[0]), int(date[1]), int( date[2])))
                item["link"] = index.xpath("a/@href").extract()[0]
                item["title"] = index.xpath("a/text()").extract()[0]
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item
            a = response.css('div[id="AspNetPager1"]').css('a')
            url = a[-2].xpath("@href").extract()[0]
            yield Request(url, callback = self.parse)
        except:
            pass
