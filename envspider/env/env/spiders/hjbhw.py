# -*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city


class HjbhwSpider(scrapy.Spider):
    name = "hjbhw"
    allowed_domains = ["hjbhw.com"]
    start_urls = (
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=87',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=14',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=13',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=9',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=80',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=10',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=81',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=86',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=82',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=11',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=84',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=87',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=12',
            'http://www.hjbhw.com/TradeNews/TradeCatalogList.aspx?catalogid=85',
    )

    def parse(self, response):
        try:
            link_head = "http://www.hjbhw.com"
            span = response.css('span[class="text"]')
            a = response.css('table[class="text"]').css('a[target="_blank"]')
            for index in range(0, len(span)):
                item = EnvItem()
                date_info = span[index].xpath("text()").extract()[0].split()[0].split("-")
                date = "%04d-%02d-%02d"%(int(date_info[0]), int(date_info[1]), int(date_info[2]))
                item["date"] = str(date)
                item["title"] = a[index].xpath("text()").extract()[0].strip("\r\n").strip("\t").strip("\r\n")
                item["link"] = link_head + a[index].xpath("@href").extract()[0][2:]
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item
            mypager = response.css('a[class="mypager"]')
            if mypager:
                url = link_head + mypager[-2].xpath("@href").extract()[0]
                yield Request(url, callback = self.parse)
        except:
            pass
