# -*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city


class QqSpider(scrapy.Spider):
    name = "qq"
    allowed_domains = ["green.news.qq.com", "news.qq.com"]
    start_urls = (
             'http://news.qq.com/l/green/list20100329114428.htm',
             'http://news.qq.com/l/green/Gindustry/list20100329151217.htm',
             'http://news.qq.com/l/green/Gview/list20100329151304.htm',
             'http://news.qq.com/l/green/Gevent/list20100329152048.htm',
             'http://news.qq.com/l/green/keypoint/list20100329151746.htm',
             'http://news.qq.com/l/green/Gphoto/list20100329172425.htm',
    )

    def parse(self, response):
        try:
            newslist = response.css('div[class="mod newslist"]')
            for ul in newslist.xpath('ul'):
                for li in ul.xpath('li'):
                    item = EnvItem()
                    item["link"] = li.xpath("a/@href").extract()[0]
                    link_info = li.xpath("a/@href").extract()[0].split("/")
                    if link_info[2] == "news.qq.com" and link_info[3] == "a":
                        item["date"] = link_info[4][0:4] + '-' + link_info[4][4:6] + '-' + link_info[4][6:] 
                    else:
                        continue
                    item["title"] = li.xpath("a/text()").extract()[0]
                    (province, city) = get_province_city(item["title"])
                    item["province"] = province
                    item["city"] = city
                    yield item

            a = response.css('a[class="f12"]')
            if a:
                url = a.xpath("@href").extract()[-1]
                yield Request(url, callback = self.parse)
        except:
            pass
