 #-*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city
import string

class ZhbSpider(scrapy.Spider):
    name = "zhb"
    allowed_domains = ["zhb.gov.cn"]
    start_urls = (
        'http://www.zhb.gov.cn/zhxx/hjyw/',
    )
    
    def parse(self, response):
        try:
            newslistlb = response.css('td[class="hh14"]')
            for li in newslistlb.xpath("script"):
                line_list=li.xpath("text()").extract()[0].strip().split("\n")
                item = EnvItem()
                item["link"] = line_list[2].split("=")[1].split('\"')[1]
                item["title"] = line_list[0].split("=")[1].split('\'')[1]
                tmp_date = str(item["link"].split("/")[-1][1:9])
                item["date"] = '%s-%s-%s' %(tmp_date[0:4],tmp_date[4:6],tmp_date[6:8])
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item
            a = response.css('td[class="zhlbjg12"]')
            if a:
                current_index = a.xpath("script").extract()[0].split("\n")[2].split("=")[1].split(";")[0].strip()
                next_index = string.atoi(current_index)+1
                url = "http://www.zhb.gov.cn/zhxx/hjyw/index_%d.htm" %(next_index)
                yield Request(url, callback = self.parse)
        except:
            pass
