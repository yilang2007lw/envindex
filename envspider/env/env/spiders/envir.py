# -*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city
import time
import datetime
import string

def get_start_urls(start_date,end_date):
    start_time = datetime.datetime.strptime(start_date,'%Y-%m-%d')
    end_time = datetime.datetime.strptime(end_date,'%Y-%m-%d')
    delta_time = datetime.timedelta(days=1)
    while start_time <= end_time:
        date_str=start_time.strftime('%Y-%m-%d')
        year = date_str[0:4]
        month = string.atoi(date_str[5:7])
        day = string.atoi(date_str[8:10])
        date_str_2='%s-%d-%d' %(year,month,day)
        start_time = start_time + delta_time
        tmp_url='http://www.envir.gov.cn/info/index.asp?rsDate=%s&month=%s&year=%s' %(date_str_2,month,year)
        yield tmp_url

class EnvirSpider(scrapy.Spider):
    name = "envir"
    allowed_domains = ["envir.gov.cn"]
    url_list = get_start_urls("2008-01-01","2010-12-31")
    start_urls = tuple(url_list)
#    start_urls = ('http://www.envir.gov.cn/info/index.asp?rsDate=2013-12-23&month=12&year=2013',)

    def parse(self, response):
        try:
            newslistlb = response.css('a[class="xm"]')
            tmp_str=response.url.split('=')[1].split('&')[0].split('-')
            tmp_date = '%04d-%02d-%02d' %(string.atoi(tmp_str[0]),string.atoi(tmp_str[1]),string.atoi(tmp_str[2]))
            for li in newslistlb:
                item = EnvItem()

                item["link"] = "http://www.envir.gov.cn"+ li.xpath("@href").extract()[0]
                item["title"] = li.xpath("text()").extract()[0]
                #tmp_arr=item["link"].split("/")
                #item["date"] = str("-".join(tmp_arr[-3:-1])+"-"+tmp_arr[-1][2:4])
                item["date"] = str(tmp_date)
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item
        except:
            pass




