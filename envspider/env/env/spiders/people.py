# -*- coding: utf-8 -*-
import scrapy
from env.items import EnvItem
from scrapy.http import Request
from env.city import get_province_city


class PeopleSpider(scrapy.Spider):
    name = "people"
    allowed_domains = ["people.com.cn"]
    start_urls = (
            'http://env.people.com.cn/GB/74877/index.html',
            'http://env.people.com.cn/GB/193224/index.html',
            'http://env.people.com.cn/GB/1074/index.html',
            'http://env.people.com.cn/GB/1073/index.html',
            'http://env.people.com.cn/GB/219130/index.html',
            'http://env.people.com.cn/GB/36686/index.html',
            'http://env.people.com.cn/GB/35525/index.html',
            'http://env.people.com.cn/GB/1075/index.html',
    )

    def parse(self, response):
        link_head = "http://env.people.com.cn"
        try:
            list14 = response.css('ul[class="list14"]')
            for li in list14.xpath('li'):
                item = EnvItem()
                item["link"]  = link_head + li.xpath("a/@href").extract()[-1]
                link_info = li.xpath("a/@href").extract()[-1].split("/")
                item["date"] = link_info[2]+ '-'+ link_info[3][0:2] + '-' + link_info[3][2:]
                item["title"] = li.xpath("a/text()").extract()[0]
                (province, city) = get_province_city(item["title"])
                item["province"] = province
                item["city"] = city
                yield item

            fanye = response.css('div[class="fanye"]')
            if fanye.xpath("a/text()").extract():
                url = "/".join(response.url.split("/")[:-1]) + "/" + fanye.xpath("a/@href").extract()[-1]
                yield Request(url, callback = self.parse)
        except:
            pass
