#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3
import json
import os
import urllib2
import time
print "start create sqlite3 DB"
##create or open sqlite3 db
cx = sqlite3.connect("./env.db")

## define a cursor
cu = cx.cursor() 

def create_table(cx,cu,table_name):
    try:
        cu.execute('create table %s ( id integer PRIMARY KEY autoincrement,title varchar(128), link varchar(256) UNIQUE, date DATE,province varchar(32),city varchar(32))' % table_name)
    except:
        print table_name

def insert_data_to_table(cx,cu,insert_sql):
    try:
        cu.execute(insert_sql)
    except:
        pass


#cu.execute("insert into env_info values('title1', 'link1', '2014-12-23','jiangsu','nanjing')")  
#cu.execute("insert into env_info values('title2', 'link2', '2014-12-24','zhejiang','ningbo')") 
#cx.commit() 
def select_data_from_table(cx,cu,select_sql):
    #cu.execute("select * from env_info") 
    cu.execute(select_sql) 
    res=cu.fetchall()
    return res

    
create_table(cx,cu,"env_info")

#insert_sql="insert into env_info (title,link,date,province,city)values('title1', 'link1', '2014-12-23','jiangsu','nanjing')"
#insert_data_to_table(cx,cu,insert_sql)
#insert_sql2="insert into env_info (title,link,date,province,city)values('title2', 'link2', '2014-12-24','zhejiang','ningbo')"
#insert_data_to_table(cx,cu,insert_sql2)



def read_filename(filename):
    with open(filename) as file_fp:
        lines = file_fp.read()

    st=json.loads(lines.decode("utf8"));
    for item in st:
        insert_sql = "insert into env_info (title,link,date,province,city)values(\'%s\',\'%s\',\'%s\',\'%s\',\'%s\')" %(item['title'],item['link'],item['date'],item['province'],item['city'])
        insert_data_to_table(cx,cu,insert_sql)
    cx.commit()


def query_sqlite3_db_count(start_date,end_date,title_key):
    query_sql="select count(*) from env_info where title like \'%s\' and date between \"%s\"  and \"%s\"" %('%'+title_key+'%',start_date,end_date)
    print query_sql
    st=select_data_from_table(cx,cu,query_sql);
    print st

#query_sqlite3_db_count("2014-12-01","2014-12-24","北京")
#filename_list=["people.json" , "qq_gevent.json","qq_gindustry.json","qq_gphoto.json","qq_gview.json","qq_keypoint.json","qq_list.json","sina.json"]
#filename_list=["qq_gevent.json","qq_gindustry.json","qq_gphoto.json","qq_gview.json","qq_keypoint.json","qq_list.json","sina.json"]
#filename_list=["people.json"]
filename_list = os.listdir("env/json_data")
for f_name in filename_list:
    input_name='env/json_data/'+ f_name
    print input_name;
    read_filename(input_name)
#select_sql='select * from env_info'

#select_data_from_table(cx,cu,select_sql)
    
cx.close()

