#!/usr/bin/env python
# -*- coding: utf-8 -*-

import web
import sqlite3
import datetime
provincelist = ["北京","广东","上海","天津","重庆","辽宁","江苏","湖北","四川","陕西","河北",
            "山西","河南","吉林","黑龙江","内蒙古","山东","安徽","浙江","福建","湖南",
            "广西","江西","贵州","云南","西藏","海南","甘肃","宁夏","青海","新疆","香港","澳门","台湾"]

urls = (
    "/", "index",
)

def get_keyword_data(cx, cu, start ,end, key):
    if start and end:
        query_sql = "select province from env_info where title like \'%s\' and date between \"%s\"  and \"%s\"" \
            %('%'+key+'%',start,end)
    elif start:
        query_sql = "select province from env_info where title like \'%s\' and date >= \"%s\"" \
            %('%'+key+'%',start)
    elif end:
        query_sql = "select province from env_info where title like \'%s\' and date <= \"%s\"" \
            %('%'+key+'%',end)
    else:
        query_sql = "select province from env_info where title like \'%s\'" \
            %('%'+key+'%')
    cu.execute(query_sql) 
    return cu.fetchall()


def get_data_by_year(cx,cu,key,year):
    query_sql = "select count(*) from env_info where title like \'%s\' and date between '%s-01-01' and '%s-12-31'" \
            %('%'+key+'%', year,year)
    cu.execute(query_sql) 
    return cu.fetchall()
    

def query_env(start, end, query):
    env = {}
    env["start"] = start
    env["end"] = end
    env["query"] = query
    if not query:
        return env

    env["bar"] = {}
    env["bar"]["legend_data"] = []
    env["bar"]["series_data"] = []

    env["line"] = {}
    env["line"]["legend_data"] = []
    env["line"]["series_data"] = []

    env["pie"] = {}
    env["pie"]["title"] = "环保指数展示"
    env["pie"]["sub_title"] = "饼状图"
    env["pie"]["legend_data"] = []
    env["pie"]["series_data"] = []

    env["map"] = {}
    env["map"]["title"] = "环保指数展示"
    env["map"]["sub_title"] = "省份图"
    env["map"]["max"] = 5000
    env["map"]["legend_data"] = []
    env["map"]["series_data"] = []

    cx = sqlite3.connect("./env.db")
    cu = cx.cursor() 
    for q in query.split(" "):

        env["bar"]["legend_data"].append(q)
        env["line"]["legend_data"].append(q)

        c_2008=get_data_by_year(cx,cu,q,'2008')[0][0]
        c_2009=get_data_by_year(cx,cu,q,'2009')[0][0]
        c_2010=get_data_by_year(cx,cu,q,'2010')[0][0]
        c_2011=get_data_by_year(cx,cu,q,'2011')[0][0]
        c_2012=get_data_by_year(cx,cu,q,'2012')[0][0]
        c_2013=get_data_by_year(cx,cu,q,'2013')[0][0]
        c_2014=get_data_by_year(cx,cu,q,'2014')[0][0]

        q_bar_data = {}
        q_bar_data["name"] = q
        q_bar_data["type"] = "bar"
        q_bar_data["data"] = [c_2008, c_2009, c_2010, c_2011, c_2012, c_2013, c_2014]
        env["bar"]["series_data"].append(q_bar_data)

        q_line_data = {}
        q_line_data["name"] = q
        q_line_data["type"] = "line"
        q_line_data["stack"] = "count"
        q_line_data["data"] = [c_2008, c_2009, c_2010, c_2011, c_2012, c_2013, c_2014]
        env["line"]["series_data"].append(q_line_data)
        q_data = get_keyword_data(cx, cu, start, end, q)

        env["pie"]["legend_data"].append(q)
        env["pie"]["series_data"].append({"name":q, "value":len(q_data)})

        env["map"]["legend_data"].append(q)
        q_map_data = {}
        q_map_data["name"] = q
        q_map_data["data"] = []
        for p in provincelist:
            q_map_data["data"].append({"name":p, "value":q_data.count((p.decode("utf-8"),))})

        env["map"]["series_data"].append(q_map_data)

    cx.close()
    endtime = datetime.datetime.now()
    return env

render = web.template.render('templates/', globals = {"env":query_env})

class index:

    def GET(self):
        data = web.input()
        try:
            start = data.start
            end = data.end 
            query = data.query
        except:
            start = None
            end = None
            query = None
        return render.main(query_env(start, end, query))

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
